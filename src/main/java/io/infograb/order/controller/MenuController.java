package io.infograb.order.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.infograb.order.model.Menu;
import io.infograb.order.service.MenuService;

@CrossOrigin(origins = "*")
@RestController
public class MenuController {

	private final MenuService menuService;

	@Autowired
	public MenuController(MenuService menuService) {
		this.menuService = menuService;
	}

	@GetMapping(value = "/menus")
	public List<Menu> getMenus(HttpServletRequest request) throws Exception {
		return menuService.findMenus();
	}

	@GetMapping(value = "/menus/{id}/photo", produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] getMenuPhoto(@PathVariable String id) throws IOException {
		Resource resource = new ClassPathResource("images/menu_" + id + ".png");
		InputStream inputStream = resource.getInputStream();
		return IOUtils.toByteArray(inputStream);
	}

}
