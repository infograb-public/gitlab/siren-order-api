package io.infograb.order.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import io.infograb.order.model.Menu;

import java.util.List;

@Repository
@Mapper
public interface MenuMapper {

	@Select("SELECT id, name, english_name, price FROM menu")
	List<Menu> selectMenus();

}
