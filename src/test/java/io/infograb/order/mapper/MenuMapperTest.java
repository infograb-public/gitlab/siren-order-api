package io.infograb.order.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;

import io.infograb.order.model.Menu;

@DisplayName("Test MenuMapper")
@MybatisTest
public class MenuMapperTest {

	@Autowired
	private MenuMapper menuMapper;

	@DisplayName("Test selecting all menus")
	@Test
	public void shouldReturnSelectAllMenus() {
		List<Menu> menus = menuMapper.selectMenus();
		assertNotNull(menus, "No menus found");
		assertEquals(menus.size(), 3);
	}

}
